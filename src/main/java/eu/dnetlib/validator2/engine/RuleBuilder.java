package eu.dnetlib.validator2.engine;

import java.util.Map;

public interface RuleBuilder<R extends Rule> {

    R build();

    R buildFrom(Map<String, String> map);
}
