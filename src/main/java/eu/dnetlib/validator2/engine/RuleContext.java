package eu.dnetlib.validator2.engine;

import java.util.Collection;
import java.util.Map;

public interface RuleContext extends PropertyDriven {

    String ID_PROPERTY_NAME = "id";

    RuleProperty getIdProperty();

    Collection<RuleProperty> getProperties();

    @Override
    default void readFrom(final Map<String, String> map) {
        getProperties().forEach(prop -> {
            prop.readFrom(map);
        });
    }

    @Override
    default void writeTo(final Map<String, String> map) {
        getProperties().forEach(prop -> {
            prop.writeTo(map);
        });
    }
}
