package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleContext;

public interface NodeListContext extends RuleContext {

    String PROPERTY_NAME = "xmlTraversal";

    NodeListActionProperty getNodeListActionProperty();
}
