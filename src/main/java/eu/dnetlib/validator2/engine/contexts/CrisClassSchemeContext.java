package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleContext;
import eu.dnetlib.validator2.engine.RuleProperty;

public interface CrisClassSchemeContext extends RuleContext {

    String ID_PROPERTY_NAME = "scheme_id";

    RuleProperty getCrisClassSchemeIdProperty();

}
