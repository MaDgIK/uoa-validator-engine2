package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleContext;

public interface NotConfusedFieldsContext extends RuleContext {

    String PROPERTY_NAME = "fields";

    FieldsProperty getFieldsProperty();

}
