package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.Helper;
import eu.dnetlib.validator2.engine.RuleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Predicate;

public interface CardinalityContext extends RuleContext {

    Logger logger = LoggerFactory.getLogger(CardinalityContext.class);


    String GREATER_THAN_PROPERTY_NAME = "gt";
    String LESS_THAN_PROPERTY_NAME = "lt";
    String UPPER_BOUND_PROPERTY_NAME = LESS_THAN_PROPERTY_NAME;
    String LOWER_BOUND_PROPERTY_NAME = GREATER_THAN_PROPERTY_NAME;
    String IS_INCLUSIVE_PROPERTY_NAME = "inclusive";

    LongRuleProperty getLowerBoundProperty();

    LongRuleProperty getUpperBoundProperty();

    BooleanRuleProperty getIsInclusiveProperty();

    default Predicate<Integer> cardinalityPredicate() {
        logger.debug("Evaluating cardinality " + getIdProperty().getValue());
        long min = getLowerBoundProperty().getLongValue();
        long max = getUpperBoundProperty().getLongValue();

        // TODO: [Minor] Perhaps we should disallow changing the properties
        //       of a rule's context after the rule has been created?
        return Helper.createCardinalityPredicate(min, max, getIsInclusiveProperty().isTrue());
    }

}
