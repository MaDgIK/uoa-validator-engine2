package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleProperty;

public interface LongRuleProperty extends RuleProperty {

    long getLongValue();

    void setLongValue(long value);

}
