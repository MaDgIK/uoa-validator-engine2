package eu.dnetlib.validator2.engine;

public class RuleEngine {

    public static <T, R extends Rule<T>> void applyAndReport(R rule, T t, Reporter<T, R> reporter) throws IllegalArgumentException
    {
        if (reporter == null) throw new IllegalArgumentException("Reporter cannot be null");
        if (rule == null) throw new IllegalArgumentException("Rule cannot be null");

        try {
            if ( rule.test(t) )
                reporter.reportSuccessFor(rule, t);
            else
                reporter.reportFailureFor(rule, t);
        } catch (Throwable throwable) {
            reporter.reportErrorFor(rule, t, throwable);
        }
    }

}