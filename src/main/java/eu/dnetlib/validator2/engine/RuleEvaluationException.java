package eu.dnetlib.validator2.engine;

public class RuleEvaluationException extends RuntimeException {

    public RuleEvaluationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuleEvaluationException(Throwable cause) {
        super(cause);
    }


}
