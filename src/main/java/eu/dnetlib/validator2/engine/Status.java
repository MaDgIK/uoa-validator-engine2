package eu.dnetlib.validator2.engine;

public enum Status {
    SUCCESS,
    FAILURE,
    ERROR
}
