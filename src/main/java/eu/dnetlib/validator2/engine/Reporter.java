package eu.dnetlib.validator2.engine;

import org.slf4j.LoggerFactory;

public class Reporter<T, R extends Rule<T>> {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Reporter.class);


    private final RuleDiagnostics<T, R> diagnostics;

    public Reporter(RuleDiagnostics<T, R> diagnostics) {
        this.diagnostics = diagnostics;
    }

    public void reportSuccessFor(R rule, T t) {
        try {
            diagnostics.success(rule, t);
        } catch (Throwable throwable) {
            logger.error("Failed to report success of applying " + rule + " to value: " + t, throwable);
        }
    }

    public void reportFailureFor(R rule, T t) {
        try {
            diagnostics.failure(rule, t);
        } catch (Throwable throwable) {
            logger.error("Failed to report failure of applying " + rule + " to value: " + t, throwable);
        }
    }

    public void reportErrorFor(R rule, T t, Throwable throwable) {
        try {
            diagnostics.error(rule, t, throwable);
        } catch (Throwable throwable1) {
            logger.error("Failed to report error of applying " + rule + " to value: " + t, throwable);
        }
    }

    @Override
    public String toString() {
        return "Reporter{" +
                "diagnostics=" + diagnostics +
                '}';
    }
}
