package eu.dnetlib.validator2.engine;

import java.util.function.Predicate;

public interface Rule<T> extends Predicate<T> {

    <C extends RuleContext> C getContext();

    @Override
    boolean test(T t) throws RuleEvaluationException;
}
