package eu.dnetlib.validator2.engine.builtins;

public class SimpleContext extends StandardRuleContext {

    public SimpleContext(String valueOfIdProperty) {
        getIdProperty().setValue(valueOfIdProperty);
    }

}
