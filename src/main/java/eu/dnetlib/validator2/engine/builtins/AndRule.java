package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.Predicates;
import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.RuleContext;

public class AndRule<T, C extends RuleContext> extends SimpleRule<T, C> {

    public AndRule(C ctx, Iterable<Rule<T>> rules) {
        super(ctx, Predicates.all(rules));
    }
}
