package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.contexts.TermsProperty;
import eu.dnetlib.validator2.engine.contexts.XMLCrisClassSchemeContextWithVocabulary;
import org.w3c.dom.NodeList;

import java.util.Map;

public class XMLCrisClassVocabularyRule extends XMLRule<XMLCrisClassSchemeContextWithVocabulary> {

    protected XMLCrisClassVocabularyRule(XMLCrisClassSchemeContextWithVocabulary context) {
        super(context, (NodeList nodes) -> {
            String schemeId = context.getCrisClassSchemeIdProperty().getValue();
            TermsProperty terms = context.getTermsProperty();

            /*
            TODO Implement test code
             Following original implementation, NodeListAction is applied to select nodes of NodeList
            */
            return false;
        });
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractRuleBuilder<XMLCrisClassVocabularyRule, XMLCrisClassSchemeContextWithVocabulary> {

        Builder() {
            super(new StandardCrisClassXMLContextWithVocabulary());
            initialize();
        }

        private void initialize() {
            // Terms Type Property of VocabularyContext is not used here...
            context.getTermsTypeProperty().setValue("notNull");
        }

        public Builder setId(String id) {
            context.getIdProperty().setValue(id);
            return this;
        }

        public Builder setXPathExpression(String xpath) {
            context.getXPathExpressionProperty().setValue(xpath);
            return this;
        }

        public Builder setNodeListAction(String nodeListAction) throws RuntimeException {
            context.getNodeListActionProperty().setValue(nodeListAction);
            return this;
        }

        public Builder setVocabularyTerms(String terms) {
            context.getTermsProperty().setValue(terms);
            return this;
        }

        public Builder setCrisClassSchemeId(String id) {
            context.getCrisClassSchemeIdProperty().setValue(id);
            return this;
        }

        @Override
        public XMLCrisClassVocabularyRule build() {
            ensureContextIsValid();
            return new XMLCrisClassVocabularyRule(context);
        }

        @Override
        public XMLCrisClassVocabularyRule buildFrom(Map<String, String> map) {
            context.readFrom(map);
            ensureContextIsValid();
            return new XMLCrisClassVocabularyRule(context);
        }
    }

}
