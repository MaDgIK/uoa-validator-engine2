package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.*;

abstract class AbstractRuleBuilder<R extends Rule, C extends RuleContext> implements RuleBuilder<R> {

    C context;

    AbstractRuleBuilder(C context) {
        this.context = context;
    }

    void ensureContextIsValid() {
        for (RuleProperty prop : context.getProperties()) {
            if (Helper.isEmpty(prop.getValue())) {
                throw new IllegalStateException("Empty value for property " + prop.getName());
            }
        }
    }
}
