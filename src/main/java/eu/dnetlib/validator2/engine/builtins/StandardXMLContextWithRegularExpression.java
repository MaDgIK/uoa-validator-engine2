package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleProperty;
import eu.dnetlib.validator2.engine.contexts.RegularExpressionContext;
import eu.dnetlib.validator2.engine.contexts.RegularExpressionProperty;
import eu.dnetlib.validator2.engine.contexts.XMLContextWithRegularExpression;

import java.util.ArrayList;
import java.util.Collection;

class StandardXMLContextWithRegularExpression
        extends StandardXMLContext
        implements XMLContextWithRegularExpression {

    private final StandardRegularExpressionProperty regexp =
            new StandardRegularExpressionProperty(RegularExpressionContext.PROPERTY_NAME);

    @Override
    public RegularExpressionProperty getRegularExpressionProperty() {
        return regexp;
    }

    @Override
    public Collection<RuleProperty> getProperties() {
        Collection<RuleProperty> props = new ArrayList<>(super.getProperties());
        props.add(regexp);
        return props;
    }
}
