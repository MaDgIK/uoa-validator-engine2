package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleProperty;
import eu.dnetlib.validator2.engine.contexts.FieldsProperty;
import eu.dnetlib.validator2.engine.contexts.NotConfusedFieldsContext;
import eu.dnetlib.validator2.engine.contexts.XMLContextWithNotConfusedFields;

import java.util.ArrayList;
import java.util.Collection;

class StandardXMLContextWithNotConfusedFields
        extends StandardXMLContext
        implements XMLContextWithNotConfusedFields {

    private final StandardFieldsProperty fields = new StandardFieldsProperty(NotConfusedFieldsContext.PROPERTY_NAME);

    @Override
    public FieldsProperty getFieldsProperty() {
        return fields;
    }

    @Override
    public Collection<RuleProperty> getProperties() {
        Collection<RuleProperty> props = new ArrayList<>(super.getProperties());
        props.add(fields);
        return props;
    }

}
