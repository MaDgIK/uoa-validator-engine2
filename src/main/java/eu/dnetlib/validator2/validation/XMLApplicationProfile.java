package eu.dnetlib.validator2.validation;

import eu.dnetlib.validator2.validation.guideline.Guideline;
import org.w3c.dom.Document;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * An application-specific collection of guidelines
 */
public interface XMLApplicationProfile {

    String name();

    Collection<? extends Guideline<Document>> guidelines();

    Guideline<Document> guideline(String guidelineName);

    ValidationResult validate(String id, Document document);

    ValidationResult validate(String id, List<String> feirRuleIds, Document document);

    int maxScore();


    interface ValidationResult extends Serializable {

        // Follow the bean conventions.
        String getId();

        void setId(String id);

        double getScore();

        void setScore(double score);

        Map<String, Guideline.Result> getResults();

        void setResults(Map<String, Guideline.Result> results);
    }

}
