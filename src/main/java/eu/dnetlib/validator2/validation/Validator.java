package eu.dnetlib.validator2.validation;

import eu.dnetlib.validator2.validation.task.ValidationTaskOutput;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public interface Validator<T> {

    default void validate(T subject, Consumer<ValidationTaskOutput> outputConsumer) {
        validate(Executors.newSingleThreadExecutor(), subject, outputConsumer);
    }

    void validate(ExecutorService executor, T subject, Consumer<ValidationTaskOutput> outputConsumer);
}
