package eu.dnetlib.validator2.validation.utils;

import java.util.function.Predicate;

public class ISO639ValuePredicate implements Predicate<String> {

    @Override
    public boolean test(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }

        return ISOLangCodes.contains(s);
    }
}
