package eu.dnetlib.validator2.validation.utils;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class RegexValuePredicate implements Predicate<String> {
    private Pattern pattern;

    public RegexValuePredicate(Pattern pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean test(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }
        return pattern.matcher(s).matches();
    }
}
