package eu.dnetlib.validator2.validation.utils;

import java.util.function.Predicate;

import static eu.dnetlib.validator2.validation.utils.SupportedRegExs.COMPILED_EMBARGOED_END_DATE_REG_EX;

public class EmbargoedEndDateValuePredicate implements Predicate<String> {

    @Override
    public boolean test(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }
        //TODO: validate date?
        return COMPILED_EMBARGOED_END_DATE_REG_EX.matcher(s).matches();
    }
}
