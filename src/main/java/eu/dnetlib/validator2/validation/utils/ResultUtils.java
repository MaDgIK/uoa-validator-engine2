package eu.dnetlib.validator2.validation.utils;

import eu.dnetlib.validator2.engine.Status;
import eu.dnetlib.validator2.validation.guideline.Guideline;

import java.util.List;

public class ResultUtils {


	public static Guideline.Result getNewResult(List<String> warnings2, List<String> errors2, int score2) {

		final Guideline.Result result = new Guideline.Result() {

			private int score;
			private Status status;
			private List<String> warnings;
			private List<String> errors;
			private String internalError;


			@Override
			public int getScore() {
				return this.score;
			}

			@Override
			public void setScore(int score) {
				this.score = score;
			}

			@Override
			public Status getStatus() {
				return this.status;
			}

			@Override
			public void setStatus(Status status) {
				this.status = status;
			}

			@Override
			public List<String> getWarnings() {
				return this.warnings;
			}

			@Override
			public void setWarnings(List<String> warnings) {
				this.warnings = warnings;
			}

			@Override
			public List<String> getErrors() {
				return this.errors;
			}

			@Override
			public void setErrors(List<String> errors) {
				this.errors = errors;
			}

			@Override
			public String getInternalError() {
				return this.internalError;
			}

			@Override
			public void setInternalError(String internalError) {
				this.internalError = internalError;
			}
		};

		result.setScore(score2);
		result.setStatus(Status.valueOf((score2 > 0) ? "SUCCESS" : "FAILURE"));
		result.setWarnings(warnings2);
		result.setErrors(errors2);

		return result;
	}

}
