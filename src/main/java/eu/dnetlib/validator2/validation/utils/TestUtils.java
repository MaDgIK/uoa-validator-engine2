package eu.dnetlib.validator2.validation.utils;

import org.slf4j.LoggerFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class TestUtils {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TestUtils.class);

    public static final String TEST_FILES_BASE_DIR = "src/test/resources/";


    public static DocumentBuilder getDocumentBuilder()
    {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setValidating(false);
            documentBuilderFactory.setNamespaceAware(true);
            documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false);
            return documentBuilderFactory.newDocumentBuilder();
        } catch (Exception e) {
            logger.error("", e);
            return null;
        }
    }

}
