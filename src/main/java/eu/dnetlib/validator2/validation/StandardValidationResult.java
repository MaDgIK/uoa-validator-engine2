package eu.dnetlib.validator2.validation;

import eu.dnetlib.validator2.validation.guideline.Guideline;

import java.util.Map;


public class StandardValidationResult implements XMLApplicationProfile.ValidationResult {

    private String id;
    private double score;
    private Map<String, Guideline.Result> results;


    public StandardValidationResult() {
    }

    public StandardValidationResult(String id, double score, Map<String, Guideline.Result> results) {
        this.id = id;
        this.score = score;
        this.results = results;
    }


    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public double getScore() {
        return score;
    }

    @Override
    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public Map<String, Guideline.Result> getResults() {
        return results;
    }

    @Override
    public void setResults(Map<String, Guideline.Result> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "StandardValidationResult{" +
                "id='" + id + '\'' +
                ", score=" + score +
                ", results=" + results +
                '}';
    }
}
