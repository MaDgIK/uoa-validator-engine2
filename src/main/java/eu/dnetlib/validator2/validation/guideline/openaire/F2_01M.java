package eu.dnetlib.validator2.validation.guideline.openaire;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.SyntheticGuideline;
import eu.dnetlib.validator2.validation.utils.RegexValuePredicate;
import org.w3c.dom.Document;

import java.util.*;
import java.util.stream.Collectors;

import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE;
import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE_TO_N;
import static eu.dnetlib.validator2.validation.utils.SupportedRegExs.COMPILED_ISO_8601_DATE_REG_EX;

public final class F2_01M extends AbstractOpenAireProfile {

    private static final String[] TITLE_TYPES = {
            "AlternativeTitle", "Subtitle", "TranslatedTitle", "Other"
    };

    private static final String[] NAME_TYPES = {
            "Organizational", "Personal"
    };

    private static final ElementSpec F2_01M_SPEC_1 = Builders.
        forMandatoryElement("creator", ONE_TO_N)
            .withSubElement(Builders.forMandatoryElement("creatorName", ONE))
            .withSubElement(Builders
                    .forRecommendedElement("nameIdentifier")
                    .withRecommendedAttribute("nameIdentifierScheme")
                    .withRecommendedAttribute("schemeURI")
            )
            .withSubElement(Builders.forRecommendedRepeatableElement("affiliation"))
            .build();


    private static final ElementSpec F2_01M_SPEC_2 = Builders.
            forMandatoryElement("title", ONE_TO_N)
            .withOptionalAttribute("titleType", TITLE_TYPES)
            .build();

    private static final ElementSpec F2_01M_SPEC_3 = Builders.
            forMandatoryElement("publisher", ONE_TO_N).
            build();

    private static final ElementSpec F2_01M_SPEC_4 = Builders.
            forMandatoryElement("date", ONE_TO_N).allowedValues(new RegexValuePredicate(COMPILED_ISO_8601_DATE_REG_EX))
            // We need the publication date. So, according to the guidelines we use “Issued” for the date the resource is published or distributed
            .withMandatoryAttribute("dateType", "Issued")
            .build();

    private static final ElementSpec F2_01M_SPEC_5 = Builders.
            forMandatoryElement("description", ONE_TO_N).
            withMandatoryAttribute("descriptionType", "Abstract").
            build();

    private static final ElementSpec F2_01M_SPEC_6 = Builders.
            forMandatoryElement("subject", ONE_TO_N).
            withOptionalAttribute("subjectScheme").
            withOptionalAttribute("schemeURI").
            build();

    //TODO: weights for guidelines haven't been finalized. They've been given an arbitrary value of 1.
    public static SyntheticGuideline F2_01M_1 = SyntheticGuideline.of("Creator", 2, F2_01M_SPEC_1);
    public static SyntheticGuideline F2_01M_2 = SyntheticGuideline.of("Title", 2, F2_01M_SPEC_2);
    public static SyntheticGuideline F2_01M_3 = SyntheticGuideline.of("Publisher", 2, F2_01M_SPEC_3);
    public static SyntheticGuideline F2_01M_4 = SyntheticGuideline.of("Date", 2, F2_01M_SPEC_4);
    public static SyntheticGuideline F2_01M_5 = SyntheticGuideline.of("Summary (Description with descriptionType)", 2, F2_01M_SPEC_5);
    public static SyntheticGuideline F2_01M_6 = SyntheticGuideline.of("Keywords (Subject)", 2, F2_01M_SPEC_6);

    private static final List<Guideline<Document>> GUIDELINES = Collections.unmodifiableList(
            Arrays.asList(
                    F2_01M_1,
                    F2_01M_2,
                    F2_01M_3,
                    F2_01M_4,
                    F2_01M_5,
                    F2_01M_6
            )
    );

    private static final Map<String, Guideline> GUIDELINE_MAP = GUIDELINES.
            stream().
            collect(Collectors.toMap(Guideline::getName, (guideline) -> guideline));

    private static final int MAX_SCORE = GUIDELINES.stream().map(Guideline::getWeight).reduce(0, Integer::sum);

    public F2_01M() {
        super("Rich metadata is provided to allow discovery");
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return GUIDELINES;
    }

    /**
     *
     * @param guidelineName
     * @return
     */
    @Override
    public Guideline guideline(String guidelineName) {
        return GUIDELINE_MAP.get(guidelineName);
    }

    @Override
    public int maxScore() {
        return MAX_SCORE;
    }
}