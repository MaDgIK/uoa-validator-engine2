package eu.dnetlib.validator2.validation.guideline.openaire.cris;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.Cardinality;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import org.w3c.dom.Document;

import java.util.Collection;

public class CrisEventV111Profile extends AbstractCrisProfile {

    private static final Builders.ElementSpecBuilder TYPE_SPEC = Builders.
            forOptionalRepeatableElement("Type").
            withMandatoryAttribute("scheme");

    private static final Builders.ElementSpecBuilder PLACE_SPEC = Builders.
            forOptionalElement("Place");

    private static final Builders.ElementSpecBuilder COUNTRY_SPEC = Builders.
            forOptionalElement("Country");

    //Similar 3 elements below. Extract method maybe?
    private static final Builders.ElementSpecBuilder ORGANIZER_SPEC = Builders.
            forOptionalRepeatableElement("Organizer").
            //TODO: Optional 1 of 2 (OrgUnit, Project)
            withSubElement(ORG_UNIT_SPEC). //TODO: Pass proper spec
            withSubElement(PROJECT_SPEC); //TODO: Pass proper spec

    private static final Builders.ElementSpecBuilder SPONSOR_SPEC = Builders.
            forOptionalRepeatableElement("Sponsor").
            //TODO: Optional 1 of 2 (OrgUnit, Project)
            withSubElement(ORG_UNIT_SPEC). //TODO: Pass proper spec
            withSubElement(PROJECT_SPEC); //TODO: Pass proper spec

    private static final Builders.ElementSpecBuilder PARTNER_SPEC = Builders.
            forOptionalRepeatableElement("Partner").
            //TODO: Optional 1 of 2 (OrgUnit, Project)
            withSubElement(ORG_UNIT_SPEC). //TODO: Pass proper spec
            withSubElement(PROJECT_SPEC); //TODO: Pass proper spec

    private static final ElementSpec EVENT_SPEC = Builders.
            forMandatoryElement("Event", Cardinality.ONE).
            withMandatoryAttribute("id").
            withSubElement(TYPE_SPEC).
            withSubElement(ACRONYM_SPEC).
            withSubElement(NAME_SPEC).
            withSubElement(PLACE_SPEC).
            withSubElement(COUNTRY_SPEC).
            withSubElement(START_DATE_SPEC).
            withSubElement(END_DATE_SPEC).
            withSubElement(DESCRIPTION_SPEC).
            withSubElement(SUBJECT_SPEC).
            withSubElement(KEYWORD_SPEC).
            withSubElement(ORGANIZER_SPEC).
            withSubElement(SPONSOR_SPEC).
            withSubElement(PARTNER_SPEC).
            build();

    public CrisEventV111Profile(String name) {
        super(name);
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return null;
    }

    @Override
    public Guideline<Document> guideline(String guidelineName) {
        return null;
    }

    @Override
    public int maxScore() {
        return 0;
    }
}
