package eu.dnetlib.validator2.validation.guideline.openaire.cris;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.Cardinality;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import org.w3c.dom.Document;

import java.util.Collection;

public class CrisServiceV111Profile extends AbstractCrisProfile {

    private static final String[] COMPATIBILITY_VOCABULARY = {
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Service_Compatibility#1.1",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Service_Compatibility#1.0"
    };

    //TODO: Add namespace?
    private static final Builders.ElementSpecBuilder COMPATIBILITY_SPEC = Builders.
            forOptionalRepeatableElement("Compatibility").
            allowedValues(COMPATIBILITY_VOCABULARY);

    private static final Builders.ElementSpecBuilder WEBSITE_URL_SPEC = Builders.
            forOptionalElement("WebsiteURL");

    private static final Builders.ElementSpecBuilder OAIPMH_BASE_URL_SPEC = Builders.
            forOptionalElement("OAIPMHBaseURL");

    private static final Builders.ElementSpecBuilder SUBJECT_HEADINGS_URL = Builders.
            forOptionalRepeatableElement("SubjectHeadingsURL");

    private static final Builders.ElementSpecBuilder OWNER_SPEC = Builders.
            forOptionalRepeatableElement("Owner").
            withSubElement(Builders.
                    forOptionalElement("DisplayName")).
            //TODO: Optional 1 of 2 (OrgUnit, Person)
            withSubElement(ORG_UNIT_SPEC). //TODO: Pass proper spec
            withSubElement(PERSON_SPEC); //TODO: Pass proper spec

    private static final ElementSpec SERVICE_SPEC = Builders.
            forMandatoryElement("Service", Cardinality.ONE).
            withMandatoryAttribute("id").
            withSubElement(COMPATIBILITY_SPEC).
            withSubElement(ACRONYM_SPEC).
            withSubElement(NAME_SPEC).
            withSubElement(IDENTIFIER_SPEC).
            withSubElement(DESCRIPTION_SPEC).
            withSubElement(WEBSITE_URL_SPEC).
            withSubElement(OAIPMH_BASE_URL_SPEC).
            withSubElement(SUBJECT_HEADINGS_URL).
            withSubElement(OWNER_SPEC).
            build();

    public CrisServiceV111Profile(String name) {
        super(name);
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return null;
    }

    @Override
    public Guideline<Document> guideline(String guidelineName) {
        return null;
    }

    @Override
    public int maxScore() {
        return 0;
    }
}
