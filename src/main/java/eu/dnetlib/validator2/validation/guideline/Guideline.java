package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Status;

import java.io.Serializable;
import java.util.List;

public interface Guideline<T> {

    String getName();

    String getFairRuleId();

    String getDescription();

    String getLink();

    String getFairPrinciples();

    int getWeight(); //that's the "score" of the guideline if it succeeds

    RequirementLevel getRequirementLevel();

    default Result validate(T t) {
        return validate(t == null ? "Object" : t.getClass().getSimpleName(), t);
    }

    Result validate(String id, T t);

    interface Result extends Serializable {
        
        // Follow the bean conventions.
        int getScore();
        void setScore(int score);

        Status getStatus();
        void setStatus(Status status);


        // When status == SUCCESS, potential warnings are held here
        // This may also contain messages when status == FAILURE
        List<String> getWarnings();
        void setWarnings(List<String> warnings);


        // When status == FAILURE, the errors are held here
        // We currently hold a single error (and fail fast)
        List<String> getErrors();
        void setErrors(List<String> errors);


        // When status == ERROR, the internal error is held here
        String getInternalError();
        void setInternalError(String internalError);
    }

}
