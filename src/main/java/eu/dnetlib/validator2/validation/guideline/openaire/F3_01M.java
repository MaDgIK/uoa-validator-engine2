package eu.dnetlib.validator2.validation.guideline.openaire;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.SyntheticGuideline;
import org.w3c.dom.Document;

import java.util.*;
import java.util.stream.Collectors;

import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE;
import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE_TO_N;

public final class F3_01M extends AbstractOpenAireProfile {

    private static final String[] RESOURCE_IDENTIFIER_TYPES = {
            "ARK", "DOI", "Handle", "IGSN", "arXiv", "PURL", "URL", "URN", "PMID"
    };

    private static final String[] PERSISTENT_IDENTIFIER_TYPES = {
            "IGSN", "QID", "ARK", "IVOA", "CAS RN", "RRID", "URL", "RInChi", "ERM", "ISO 27729:2012 ISNI",
            "EC Number", "PURL", "LSID", "TFClass Schema", "InChI", "w3id", "Handle", "DOI", "ORCID iD", "arXiv"
    };
 
    private static final ElementSpec F3_01M_SPEC_1 = Builders.
            forMandatoryElement("identifier", ONE_TO_N).
//            withMandatoryAttribute("identifierType", new PIDCheckValuePredicate()).
            withMandatoryAttribute("identifierType", RESOURCE_IDENTIFIER_TYPES).
            build();

    private static final ElementSpec F3_01M_SPEC_2 = Builders.
            forMandatoryElement("alternateIdentifier", ONE_TO_N).
//            withMandatoryAttribute("identifierType", new PIDCheckValuePredicate()).
        withMandatoryAttribute("identifierType", PERSISTENT_IDENTIFIER_TYPES).
            build();

    //TODO: weights for guidelines haven't been finalized. They've been given an arbitrary value of 1.
    public static SyntheticGuideline F3_01M_1 = SyntheticGuideline.of("Identifier", 2, F3_01M_SPEC_1);
    public static SyntheticGuideline F3_01M_2 = SyntheticGuideline.of("AlternateIdentifier", 2, F3_01M_SPEC_2);
    private static final List<Guideline<Document>> GUIDELINES = Collections.unmodifiableList(
            Arrays.asList(
                    F3_01M_1,
                    F3_01M_2
            )
    );

    private static final Map<String, Guideline> GUIDELINE_MAP = GUIDELINES.
            stream().
            collect(Collectors.toMap(Guideline::getName, (guideline) -> guideline));

    private static final int MAX_SCORE = GUIDELINES.stream().map(Guideline::getWeight).reduce(0, Integer::sum);

    public F3_01M() {
        super("Metadata includes the identifier for the data");
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return GUIDELINES;
    }

    /**
     *
     * @param guidelineName
     * @return
     */
    @Override
    public Guideline guideline(String guidelineName) {
        return GUIDELINE_MAP.get(guidelineName);
    }

    @Override
    public int maxScore() { return MAX_SCORE; }
}