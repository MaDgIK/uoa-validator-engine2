package eu.dnetlib.validator2.validation.guideline.openaire;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.SyntheticGuideline;
import eu.dnetlib.validator2.validation.utils.MediaTypesValuePredicate;
import org.w3c.dom.Document;

import java.util.*;
import java.util.stream.Collectors;

import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE;
import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE_TO_N;

public final class R1_01M_LIT extends AbstractOpenAireProfile {

    private static final String[] RELATED_RESOURCE_GENERAL_TYPES = {
            "Audiovisual", "Collection", "DataPaper", "Dataset", "Event", "Image", "InteractiveResource",
            "Model", "PhysicalObject", "Service", "Software", "Sound", "Text", "Workflow", "Other"
    };

    private static final String[] RESOURCE_CONCEPT_URIS = {
            "http://purl.org/coar/resource_type/c_1162", "http://purl.org/coar/resource_type/c_6501",
            "http://purl.org/coar/resource_type/c_545b", "http://purl.org/coar/resource_type/c_b239",
            "http://purl.org/coar/resource_type/c_2df8fbb1", "http://purl.org/coar/resource_type/c_dcae04bc",
            "http://purl.org/coar/resource_type/c_beb9", "http://purl.org/coar/resource_type/c_3e5a",
            "http://purl.org/coar/resource_type/c_ba08", "http://purl.org/coar/resource_type/c_3248",
            "http://purl.org/coar/resource_type/c_2f33", "http://purl.org/coar/resource_type/c_86bc",
            "http://purl.org/coar/resource_type/c_816b", "http://purl.org/coar/resource_type/c_8042",
            "http://purl.org/coar/resource_type/c_71bd", "http://purl.org/coar/resource_type/c_18gh",
            "http://purl.org/coar/resource_type/c_18ws", "http://purl.org/coar/resource_type/c_18hj",
            "http://purl.org/coar/resource_type/c_18op", "http://purl.org/coar/resource_type/c_186u",
            "http://purl.org/coar/resource_type/c_18wq", "http://purl.org/coar/resource_type/c_18wz",
            "http://purl.org/coar/resource_type/c_18ww", "http://purl.org/coar/resource_type/c_efa0",
            "http://purl.org/coar/resource_type/c_baaf", "http://purl.org/coar/resource_type/c_ba1f",
            "http://purl.org/coar/resource_type/c_93fc", "http://purl.org/coar/resource_type/c_15cd",
            "http://purl.org/coar/resource_type/c_18co", "http://purl.org/coar/resource_type/c_18cp",
            "http://purl.org/coar/resource_type/c_6670", "http://purl.org/coar/resource_type/c_5794",
            "http://purl.org/coar/resource_type/c_c94f", "http://purl.org/coar/resource_type/c_f744",
            "http://purl.org/coar/resource_type/c_7a1f", "http://purl.org/coar/resource_type/c_bdcc",
            "http://purl.org/coar/resource_type/c_db06", "http://purl.org/coar/resource_type/c_46ec",
            "http://purl.org/coar/resource_type/c_0857", "http://purl.org/coar/resource_type/c_8544",
            "http://purl.org/coar/resource_type/c_18cf", "http://purl.org/coar/resource_type/c_18cw",
            "http://purl.org/coar/resource_type/c_18cd", "http://purl.org/coar/resource_type/c_18cc",
            "http://purl.org/coar/resource_type/c_12ce", "http://purl.org/coar/resource_type/c_8a7e",
            "http://purl.org/coar/resource_type/c_ecc8", "http://purl.org/coar/resource_type/c_c513",
            "http://purl.org/coar/resource_type/c_12cd", "http://purl.org/coar/resource_type/c_12cc",
            "http://purl.org/coar/resource_type/c_5ce6", "http://purl.org/coar/resource_type/c_ddb1",
            "http://purl.org/coar/resource_type/c_e9a0", "http://purl.org/coar/resource_type/c_7ad9",
            "http://purl.org/coar/resource_type/c_393c", "http://purl.org/coar/resource_type/c_1843"
    };

    private static final String[] RESOURCE_GENERAL_TYPES = {
            "literature", "dataset", "software", "other research product"
    };

    private static final ElementSpec R1_01M_LIT_SPEC_1 = Builders.
            forMandatoryElement("oaire:resourceType", ONE).
            withMandatoryAttribute("resourceTypeGeneral", RESOURCE_GENERAL_TYPES).
            withMandatoryAttribute("uri", RESOURCE_CONCEPT_URIS).
            build();

    private static final ElementSpec R1_01M_LIT_SPEC_2 = Builders.
            forMandatoryElement("format", ONE).
            allowedValues(new MediaTypesValuePredicate()).
            build();

//    Method for data Not applicable in Literature
//    private static final ElementSpec R1_01M_LIT_SPEC_3 = Builders. // With very low weight
//            forMandatoryElement("dc:description", ONE_TO_N).
//            withOptionalAttribute("xml:lang", new RegexValuePredicate(COMPILED_BCP47_LANG_TAGS_REG_EX)).
//            build();

    private static final ElementSpec R1_01M_LIT_SPEC_4 = Builders. // With very low weight
            forMandatoryElement("size", ONE_TO_N).
            build();

    //TODO: weights for guidelines haven't been finalized. They've been given an arbitrary value of 1.
    public static SyntheticGuideline R1_01M_LIT_1 = SyntheticGuideline.of("ResourceType", 3, R1_01M_LIT_SPEC_1);
    public static SyntheticGuideline R1_01M_LIT_2 = SyntheticGuideline.of("Format", 3, R1_01M_LIT_SPEC_2);
//    public static SyntheticGuideline R1_01M_LIT_3 = SyntheticGuideline.of("Description", 3, R1_01M_SPEC_3);
    public static SyntheticGuideline R1_01M_LIT_4 = SyntheticGuideline.of("Size", 3, R1_01M_LIT_SPEC_4);

    private static final List<Guideline<Document>> GUIDELINES = Collections.unmodifiableList(
            Arrays.asList(
                    R1_01M_LIT_1,
                    R1_01M_LIT_2,
//                    R1_01M_LIT_3,
                    R1_01M_LIT_4
            )
    );

    private static final Map<String, Guideline> GUIDELINE_MAP = GUIDELINES.
            stream().
            collect(Collectors.toMap(Guideline::getName, (guideline) -> guideline));

    private static final int MAX_SCORE = GUIDELINES.stream().map(Guideline::getWeight).reduce(0, Integer::sum);

    public R1_01M_LIT() {
        super("Plurality of accurate and relevant attributes are provided to allow reuse");
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return GUIDELINES;
    }

    /**
     *
     * @param guidelineName
     * @return
     */
    @Override
    public Guideline guideline(String guidelineName) {
        return GUIDELINE_MAP.get(guidelineName);
    }

    @Override
    public int maxScore() {
        return MAX_SCORE;
    }
}