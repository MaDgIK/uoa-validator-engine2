package eu.dnetlib.validator2.validation.guideline;

public abstract class AbstractGuideline<T> implements Guideline<T> {

    private final String name;
    private String fairRuleId;
    private String description;
    private String link;
    private String fairPrinciples;
    private final int weight;
    private RequirementLevel requirementLevel;

    public AbstractGuideline(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public AbstractGuideline(String name, String description, String link, int weight, RequirementLevel requirementLevel) {
        this.name = name;
        this.description = description;
        this.link = link;
        this.weight = weight;
        this.requirementLevel = requirementLevel;
    }

    public AbstractGuideline(String name, String description, String link, String fairPrinciples, int weight, RequirementLevel requirementLevel) {
        this.name = name;
        this.description = description;
        this.link = link;
        this.fairPrinciples = fairPrinciples;
        this.weight = weight;
        this.requirementLevel = requirementLevel;
    }

    public AbstractGuideline(String name, String fairRuleId, String description, String link, String fairPrinciples, int weight, RequirementLevel requirementLevel) {
        this.name = name;
        this.fairRuleId = fairRuleId;
        this.description = description;
        this.link = link;
        this.fairPrinciples = fairPrinciples;
        this.weight = weight;
        this.requirementLevel = requirementLevel;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFairRuleId() { return fairRuleId; }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getLink() {
        return link;
    }

    @Override
    public String getFairPrinciples() {
        return fairPrinciples;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public RequirementLevel getRequirementLevel() {
        return requirementLevel;
    }

}
