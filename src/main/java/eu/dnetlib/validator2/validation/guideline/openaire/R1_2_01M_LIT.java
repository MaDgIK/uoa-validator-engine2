package eu.dnetlib.validator2.validation.guideline.openaire;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.builtins.XMLVocabularyRule;
import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.SyntheticGuideline;
import eu.dnetlib.validator2.validation.utils.RegexValuePredicate;
import org.w3c.dom.Document;

import java.util.*;
import java.util.stream.Collectors;

import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE;
import static eu.dnetlib.validator2.validation.guideline.Cardinality.ONE_TO_N;
import static eu.dnetlib.validator2.validation.utils.SupportedRegExs.*;

public final class R1_2_01M_LIT extends AbstractOpenAireProfile {

    private static final String[] NAME_IDENTIFIER_SCHEMES = {
            "ORCID", "ISNI", "ROR", "GRID"
    };

    private static final String[] CONTRIBUTOR_TYPES = {
            "ContactPerson", "DataCollector", "DataCurator", "DataManager", "Distributor",
            "Editor", "HostingInstitution", "Producer", "ProjectLeader", "ProjectManager", "ProjectMember",
            "RegistrationAgency", "RegistrationAuthority", "RelatedPerson", "Researcher", "ResearchGroup",
            "RightsHolder", "Sponsor", "Supervisor", "WorkPackageLeader", "Other"
    };

    private static final String[] PUBLICATION_DATE_TYPE = {
            "Issued"
    };

    private static final String[] CONTRIBUTOR_NAME_IDENTIFIER_TYPES = {
            "Funder", "FundingProgramme", "ProjectID", "Jurisdiction", "ProjectName", "ProjectAcronym"
    };

    private static final String[] IDENTIFIER_TYPES = {
            "ARK", "arXiv", "bibcode", "DOI", "EAN13", "EISSN", "Handle", "IGSN", "ISBN",
            "ISSN", "ISTC", "LISSN", "LSID", "PISSN", "PMID", "PURL", "UPC", "URL", "URN", "WOS",
    };

    private static final String[] RELATION_TYPES_LIMITED = {
            "IsSupplementTo", "Describes", "IsMetadataFor", "IsPartOf"
    };

    private static final String[] NAME_TYPES = {
            "Organizational", "Personal"
    };

    private static final String[] RESOURCE_VERSION_URIS = {
            "http://purl.org/coar/version/c_b1a7d7d4d402bcce", "http://purl.org/coar/version/c_71e4c1898caa6e32",
            "http://purl.org/coar/version/c_ab4af688f83e57aa", "http://purl.org/coar/version/c_fa2ee174bc00049f",
            "http://purl.org/coar/version/c_970fb48d4fbd8a85", "http://purl.org/coar/version/c_e19f295774971610",
            "http://purl.org/coar/version/c_dc82b40f9837b551", "http://purl.org/coar/version/c_be7fb7dd8ff6fe43"
    };
    private static final String[] RELATION_TYPES = {
            "IsCitedBy", "Cites", "IsSupplementTo", "IsSupplementedBy", "IsContinuedBy",
            "Continues", "IsDescribedBy", "Describes", "HasMetadata", "IsMetadataFor", "HasVersion",
            "IsVersionOf", "IsNewVersionOf", "IsPreviousVersionOf", "IsPartOf", "HasPart", "IsReferencedBy",
            "References", "IsDocumentedBy", "Documents", "IsCompiledBy", "Compiles", "IsVariantFormOf",
            "IsOriginalFormOf", "IsIdenticalTo", "IsReviewedBy", "Reviews", "IsDerivedFrom", "IsSourceOf",
            "IsRequiredBy", "Requires"
    };
    private static final String[] RESOURCE_VERSION_LABELS = {
            "AO", "SMUR", "AM", "P", "VoR", "CVoR", "EVoR", "NA"
    };
    private static final String[] RELATED_RESOURCE_GENERAL_TYPES = {
            "Audiovisual", "Collection", "DataPaper", "Dataset", "Event", "Image", "InteractiveResource",
            "Model", "PhysicalObject", "Service", "Software", "Sound", "Text", "Workflow", "Other"
    };

    private static final ElementSpec R1_2_01M_LIT_SPEC_1 = Builders.
            forMandatoryElement("datacite:creator", ONE_TO_N).
            withSubElement(Builders.
                    forMandatoryElement("datacite:creatorName", ONE).
                    withRecommendedAttribute("nameType", NAME_TYPES)).
            withSubElement(Builders.
                    forRecommendedElement("datacite:givenName")).
            withSubElement(Builders.
                    forRecommendedElement("datacite:familyName")).
            withSubElement(Builders.
                    forRecommendedRepeatableElement("datacite:nameIdentifier").
                    withMandatoryAttribute("nameIdentifierScheme").
                    withRecommendedAttribute("schemeURI")).
            withSubElement(Builders.
                    forRecommendedRepeatableElement("datacite:affiliation")).
            build();

    private static final ElementSpec R1_2_01M_LIT_SPEC_2 = Builders.
            forMandatoryElement("datacite:contributor", ONE_TO_N).
            withMandatoryAttribute("contributorType", CONTRIBUTOR_TYPES).
            withSubElement(Builders.
                    forMandatoryElement("datacite:contributorName", ONE).
                    withRecommendedAttribute("nameType", NAME_TYPES)).
            withSubElement(Builders.
                    forOptionalElement("datacite:familyName")).
            withSubElement(Builders.
                    forOptionalElement("datacite:givenName")).
            withSubElement(Builders.
                    forRecommendedRepeatableElement("datacite:nameIdentifier").
                    withMandatoryAttribute("nameIdentifierScheme").
                    withRecommendedAttribute("schemeURI")).
            withSubElement(Builders.
                    forRecommendedRepeatableElement("datacite:affiliation")).
            build();


    private static final ElementSpec R1_2_01M_LIT_SPEC_3 = Builders.
            forMandatoryElement("datacite:date", ONE).
            withMandatoryAttribute("dateType", PUBLICATION_DATE_TYPE).
            allowedValues(new RegexValuePredicate(COMPILED_PUBLICATION_DATE_REG_EX).or(new RegexValuePredicate(COMPILED_YYYY_MM_DD_RANGE_REGEX).or(new RegexValuePredicate(COMPILED_YEAR_YYYY_REG_EX)))).
            build();

    private static final ElementSpec R1_2_01M_LIT_SPEC_4 = Builders.
            forMandatoryElement("oaire:version", ONE).
            withMandatoryIfApplicableAttribute("uri", applicabilityRuleForURIAttributeOfResourceVersion(), RESOURCE_VERSION_URIS).
            build();

    private static final ElementSpec R1_2_01M_LIT_SPEC_5 = Builders.
            forMandatoryElement("relatedIdentifier", ONE_TO_N).
            withMandatoryAttribute("relatedIdentifierType", IDENTIFIER_TYPES).
            withMandatoryAttribute("relationType", RELATION_TYPES).
            withOptionalAttribute("relatedMetadataScheme").
            withOptionalAttribute("schemeURI").
            withOptionalAttribute("schemeType").
            withOptionalAttribute("resourceTypeGeneral", RELATED_RESOURCE_GENERAL_TYPES).
            build();

    private static Rule<Document> applicabilityRuleForURIAttributeOfResourceVersion() {
        return XMLVocabularyRule.builder().
                setId(ElementSpec.APPLICABILITY_RULE_ID).
                setXPathExpression("//*[name()='oaire:version']/text()").
                setNodeListAction("1").
                setVocabularyTermsAndTermsType(String.join(", ", RESOURCE_VERSION_LABELS), "whitelist").
                build();
    }

    //TODO: weights for guidelines haven't been finalized. They've been given an arbitrary value of 1.
    public static SyntheticGuideline R1_2_01M_LIT_1 = SyntheticGuideline.of("Creator", 2, R1_2_01M_LIT_SPEC_1);
    public static SyntheticGuideline R1_2_01M_LIT_2 = SyntheticGuideline.of("Contributor", 2, R1_2_01M_LIT_SPEC_2);
    public static SyntheticGuideline R1_2_01M_LIT_3 = SyntheticGuideline.of("Date", 2, R1_2_01M_LIT_SPEC_3);
    public static SyntheticGuideline R1_2_01M_LIT_4 = SyntheticGuideline.of("Version", 2, R1_2_01M_LIT_SPEC_4);
    public static SyntheticGuideline R1_2_01M_LIT_5 = SyntheticGuideline.of("RelatedIdentifier", 2, R1_2_01M_LIT_SPEC_5);

    private static final List<Guideline<Document>> GUIDELINES = Collections.unmodifiableList(
            Arrays.asList(
                    R1_2_01M_LIT_1,
                    R1_2_01M_LIT_2,
                    R1_2_01M_LIT_3,
                    R1_2_01M_LIT_4,
                    R1_2_01M_LIT_5
            )
    );

    private static final Map<String, Guideline> GUIDELINE_MAP = GUIDELINES.
            stream().
            collect(Collectors.toMap(Guideline::getName, (guideline) -> guideline));

    private static final int MAX_SCORE = GUIDELINES.stream().map(Guideline::getWeight).reduce(0, Integer::sum);

    public R1_2_01M_LIT() {
        super("Metadata includes provenance information according to a cross-community language");
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return GUIDELINES;
    }

    /**
     *
     * @param guidelineName
     * @return
     */
    @Override
    public Guideline guideline(String guidelineName) {
        return GUIDELINE_MAP.get(guidelineName);
    }

    @Override
    public int maxScore() {
        return MAX_SCORE;
    }
}