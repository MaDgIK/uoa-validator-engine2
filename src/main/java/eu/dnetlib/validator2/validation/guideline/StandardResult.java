package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Status;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public final class StandardResult implements Guideline.Result, Serializable {

    private static final List<String> EMPTY = Collections.emptyList();

    private List<String> warnings;
    private List<String> errors;
    private String internalError;
    private Status status;
    private int score;

    public StandardResult() {
    }

    public StandardResult(int score, Status status, List<String> warnings, List<String> errors, String internalError) {
        this.status = status;
        this.score = score;
        this.warnings = warnings;
        this.errors = errors;
        this.internalError = internalError;
    }


    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public List<String> getWarnings() {
        return warnings;
    }

    @Override
    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }

    @Override
    public List<String> getErrors() {
        return errors;
    }

    @Override
    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    @Override
    public String getInternalError() {
        return internalError;
    }

    @Override
    public void setInternalError(String internalError) {
        this.internalError = internalError;
    }

    public static StandardResult forError(String message) {
        return new StandardResult(-1, Status.ERROR, EMPTY, EMPTY, message);
    }

    public static StandardResult forSuccess(int score, List<String> warnings) {
        return new StandardResult(score, Status.SUCCESS, sanitize(warnings), EMPTY, null);
        //        LEONIDAS
//        if (warnings.isEmpty()) {
//            return new StandardResult(score, Status.SUCCESS, sanitize(warnings), EMPTY, null);
//        }
//        else {
//            return new StandardResult(0, Status.SUCCESS, sanitize(warnings), EMPTY, null);
//        }
    }

    public static StandardResult forFailure(List<String> warnings, List<String> errors) {
        return new StandardResult(0, Status.FAILURE, sanitize(warnings), sanitize(errors), null);
    }

    private static List<String> sanitize(List<String> list) {
        if (list == null || list.size() == 0) return EMPTY;
        return Collections.unmodifiableList(list);
    }

    @Override
    public String toString() {
        if (status == Status.SUCCESS || status == Status.FAILURE) {
            return status + " (" + warnings.size() + " warnings) - score " + score;
        }
        else {
            return status + " (" + warnings.size() + " warnings) - " + internalError;
        }
    }
}
