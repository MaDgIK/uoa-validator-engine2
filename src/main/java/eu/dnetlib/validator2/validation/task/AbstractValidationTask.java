package eu.dnetlib.validator2.validation.task;

import eu.dnetlib.validator2.engine.Rule;

import java.util.Collection;
import java.util.function.Consumer;

abstract class AbstractValidationTask<T, R extends Rule<T>> implements ValidationTask<T, R> {

    protected final Collection<R> rules;
    protected final T subject;

    protected AbstractValidationTask(T subject, Collection<R> rules) {
        this.subject = subject;
        this.rules = rules;
    }

    @Override
    public Collection<R> ruleSet() {
        return rules;
    }

    @Override
    public T subject() {
        return subject;
    }

    @Override
    public void run(Consumer<ValidationTaskOutput> outputConsumer) {
        ValidatorDiagnostics<T, R> outputs = new ValidatorDiagnostics<>();
        applyRulesAndReport(outputs);
        if (outputConsumer != null) {
            outputConsumer.accept(outputs);
        }
    }

    protected abstract void applyRulesAndReport(ValidatorDiagnostics<T, R> outputs);
}
