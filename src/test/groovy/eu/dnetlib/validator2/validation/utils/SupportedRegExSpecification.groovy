package eu.dnetlib.validator2.validation.utils

import spock.lang.Specification
import spock.lang.Unroll

import static eu.dnetlib.validator2.validation.utils.SupportedRegExs.*

class SupportedRegExSpecification extends Specification {

    def "Project Identifier regex"() {

        expect:
        value.matches(PROJECT_IDENTIFIER_REG_EX) == result

        where:
        value || result
        "info:eu-repo/grantAgreement/EC/FP7/244909/EU/Making Capabilities Work/WorkAble" || true
        "info:eu-repo/grantAgreement/EC/FP7/283595/EU//OpenAIREplus" || true
        "info:eu-repo/grantAgreement/EC/FP7/244909" || true
        "info:eu-repo/whatever" || false
    }

    def "License Condition regex"() {
        expect:
        COMPILED_LICENSE_CONDITION_REG_EX.matcher(value).matches() == result

        where:
        value || result
        "(c) University of Bath, 2003" || true
        "http://creativecommons.org/licenses/by-sa/2.0/uk/" || true
        "https://creativecommons.org/licenses/by-sa/2.0/uk/" || true
        "cc-by-sa, Andrew Smith" || true
        "cc-by-sa, info:eu-repo/dai/nl/344568" || true
        "cc-by-nc-sa, urn:isni:234562-2" || true
        "whatever" || false
    }

    def "Embargoed End Date regex"() {
        expect:
        COMPILED_EMBARGOED_END_DATE_REG_EX.matcher(value).matches() == result

        where:
        value || result
        "info:eu-repo/date/embargoEnd/2015-12-31" || true
        "info:eu-repo/date/embargoEnd/1979-12-31" || true
        "info:eu-repo/date/embargoEnd/1800-12-31" || true
        "whatever" || false
    }

    def "Alternative Identifier regex"() {
        expect:
        COMPILED_ALT_IDENTIFIER_REG_EX.matcher(value).matches() == result

        where:
        value || result
        "info:eu-repo/semantics/altIdentifier/doi/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/arxiv/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/ark/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/hdl/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/isbn/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/pissn/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/eissn/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/pmid/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/purl/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/urn/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/wos/XXXXXX" || true
        "info:eu-repo/semantics/altIdentifier/whatever/XXXXXX" || false
        "whatever" || false
    }

    def "Publication Reference regex"() {
        expect:
        COMPILED_PUBLICATION_REFERENCE_REG_EX.matcher(value).matches() == result

        where:
        value || result
        "info:eu-repo/semantics/reference/doi/XXXXXX" || true
        "info:eu-repo/semantics/reference/arxiv/XXXXXX" || true
        "info:eu-repo/semantics/reference/ark/XXXXXX" || true
        "info:eu-repo/semantics/reference/hdl/XXXXXX" || true
        "info:eu-repo/semantics/reference/isbn/XXXXXX" || true
        "info:eu-repo/semantics/reference/issn/XXXXXX" || true
        "info:eu-repo/semantics/reference/pmid/XXXXXX" || true
        "info:eu-repo/semantics/reference/purl/XXXXXX" || true
        "info:eu-repo/semantics/reference/urn/XXXXXX" || true
        "info:eu-repo/semantics/reference/url/XXXXXX" || true
        "info:eu-repo/semantics/reference/wos/XXXXXX" || true
        "info:eu-repo/semantics/reference/whatever/XXXXXX" || false
        "whatever" || false
    }

    def "Dataset Reference regex"() {
        expect:
        COMPILED_DATASET_REFERENCE_REG_EX.matcher(value).matches() == result

        where:
        value || result
        "info:eu-repo/semantics/dataset/ark/XXXXXX" || true
        "info:eu-repo/semantics/dataset/doi/XXXXXX" || true
        "info:eu-repo/semantics/dataset/hdl/XXXXXX" || true
        "info:eu-repo/semantics/dataset/purl/XXXXXX" || true
        "info:eu-repo/semantics/dataset/urn/XXXXXX" || true
        "info:eu-repo/semantics/dataset/url/XXXXXX" || true
        "info:eu-repo/semantics/dataset/whatever/XXXXXX" || false
        "whatever" || false
    }

    @Unroll
    def "Publication Date regex"() {
        expect:
        COMPILED_PUBLICATION_DATE_REG_EX.matcher(value).matches() == result

        where:
        value || result
        "2020-04-12" || true
        "2012-11-31T09:49:59Z" || true
        "2012-11-31T09:49:59Z" || true
        "1997-07-16T19:20:30.45+01:00" || true
        "1650" || true
        "1520-12" || true
        "1520/12" || false
        "whatever" || false
    }

    def "ISO-8601 Date regex"() {
        expect:
        COMPILED_ISO_8601_DATE_REG_EX.matcher(value).matches() == result

        where:
        value || result
        "2020-04-12" || true
        "2012-11-32T09:49:59Z" || false
        "2012-11-31T09:49:59Z" || true
        "1997-07-16T19:20:30.45+01:00" || true
        "1997-07-16T19:20:30+01:00" || true
        "1650" || true
        "1520-12" || true
        "1520/12" || false
        "0999" || true
        "whatever" || false
    }

    def "Year YYYY regex"() {
        expect:
        COMPILED_YEAR_YYYY_REG_EX.matcher(value).matches() == result

        where:
        value       || result
        "2020"      || true
        "0500"      || true
        "1650"      || true
        "0000"      || true
        "12"        || false
        "whatever"  || false
    }

    def "YYYY-MM-DD regex"() {
        expect:
        COMPILED_YYYY_MM_DD_REGEX.matcher(value).matches() == result

        where:
        value        || result
        "2020-02-12" || true
        "1997-12-31" || true
        "2001-02-29" || false
        "2001-11-31" || false
        "0850-02-19" || false
    }

    def "YYYY-MM-DD - YYYY-MM-DD regex"() {
        expect:
        COMPILED_YYYY_MM_DD_RANGE_REGEX.matcher(value).matches() == result

        where:
        value                     || result
        "2020-02-12 - 2020-02-13" || true
        "1997-12-31 - 1998-01-31" || true
        "2001-02-28 - 2001-02-29" || false
        "2001-11-31- 2001-12-01"  || false
        "1950-02-19 -1960-02-28"  || false
        "1950-02-19-1960-02-28"   || false
    }

    def "BCP47 language tags regex"() {
        expect:
        COMPILED_BCP47_LANG_TAGS_REG_EX.matcher(value).matches() == result

        where:
        value        || result
        "en-US"      || true
        "el-GR"      || true
        "en-GB"      || true
        "zh-Hant"    || true
        "a-DE"       || false
        "de-419-DE"  || false
        "invalidTag" || false
    }

}
