package eu.dnetlib.validator2.validation.utils

import spock.lang.Specification

class MediaTypesSpecification extends Specification {

    def "existance of iana media types"() {
        expect:
        MediaTypes.contains(value) == result

        where:
        value               ||  result
        "application/json"  ||  true
        "application/pdf"   ||  true
        "whatever/whenever" ||  false
        null                || false
    }
}
