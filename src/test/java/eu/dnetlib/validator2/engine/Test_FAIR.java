package eu.dnetlib.validator2.engine;

import eu.dnetlib.validator2.validation.XMLApplicationProfile;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.openaire.FAIR_Data_GuidelinesProfile;
import eu.dnetlib.validator2.validation.utils.TestUtils;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class Test_FAIR {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Test_FAIR.class);


    private static final String[] FILES = new String[]{
            "src/test/resources/openaireguidelinesV3/dia.library.tuc.gr/Record_21811.xml",
            "src/test/resources/openaireguidelinesV3/cris.vtt.fi/03.xml",
            "src/test/resources/openaireguidelinesV3/cris.vtt.fi/01_data.xml",
            "src/test/resources/openaireguidelinesV3/cris.vtt.fi/02_data.xml",
            "src/test/resources/openaireguidelinesV3/cris.vtt.fi/04_data.xml",
            "src/test/resources/openaireguidelinesV3/cris.vtt.fi/03_data.xml",
            //"src/test/resources/openaireguidelinesV4/01_gv4.xml" // This file does not exist!
    };

    public static void main(String[] args) {
        // String xmlFile = args[0];
        FAIR_Data_GuidelinesProfile profile = new FAIR_Data_GuidelinesProfile();
        logger.info("Max score: " + profile.maxScore());
        Map<String, Double> scorePerDoc = new LinkedHashMap<>();
        DocumentBuilder builder = TestUtils.getDocumentBuilder();
        if ( builder == null )
            return;
        for ( String fileName : FILES ) {
            try {
                logger.info("Processing \"" + fileName + "\"");
                Document doc = builder.parse(new File(fileName));
                XMLApplicationProfile.ValidationResult result = profile.validate(fileName, doc);
                scorePerDoc.put(fileName, result.getScore());
                Map<String, Guideline.Result> results = result.getResults();

                if ( logger.isDebugEnabled() ) {
                    for ( Map.Entry<String, Guideline.Result> entry : results.entrySet() ) {
                        logger.debug(entry.getKey() + " = " + entry.getValue());
                    }
                }
            } catch (Exception e) {
                logger.error("", e);
            }
        }

        // Individual scores
        String printout = scorePerDoc.entrySet().stream().
                map(entry -> entry.getValue() + ": " + entry.getKey()).collect(Collectors.joining("\n"));

        logger.info(printout);

        // Average Score
        OptionalDouble optionalFinalScore = scorePerDoc.values().stream().mapToDouble(aDouble -> aDouble).average();
        double finalScore = (optionalFinalScore.isPresent() ? optionalFinalScore.getAsDouble() : -1 );

        logger.info("Validation Score: " + finalScore);
    }

}
