package eu.dnetlib.validator2.engine;

import eu.dnetlib.validator2.validation.XMLApplicationProfile;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.openaire.LiteratureGuidelinesV3Profile;
import eu.dnetlib.validator2.validation.utils.TestUtils;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Test {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Test.class);


    private static final String[] FILES = new String[] {
//        "src/test/resources/openaireguidelinesV3/dia.library.tuc.gr/Record_21811.xml",
//        "src/test/resources/openaireguidelinesV3/cris.vtt.fi/01.xml",
//        "src/test/resources/openaireguidelinesV3/cris.vtt.fi/02.xml",
//        "src/test/resources/openaireguidelinesV3/cris.vtt.fi/03.xml"
            "src/test/resources/openaireguidelinesV3/ngi.brage.unit.no/ListRecords_prefix_oai_dc_set_openaire.xml"
    };

    public static void main(String[] args) {
        // String xmlFile = args[0];
        LiteratureGuidelinesV3Profile profile = new LiteratureGuidelinesV3Profile();
        logger.info("Max score: " + profile.maxScore());
        Map<String, Double> scorePerDoc = new LinkedHashMap<>();
        DocumentBuilder builder = TestUtils.getDocumentBuilder();
        if ( builder == null )
            return;
        for ( String fileName : FILES ) {
            try {
                logger.info("Processing \"" + fileName + "\"");
                Document doc = builder.parse(new File(fileName));
                XMLApplicationProfile.ValidationResult result = profile.validate(fileName, doc);
                scorePerDoc.put(fileName, result.getScore());
                Map<String, Guideline.Result> results = result.getResults();
                for ( Map.Entry<String, Guideline.Result> entry : results.entrySet() ) {
                    logger.info(entry.getKey() + " = " + entry.getValue());
                }
            } catch (Exception e) {
                logger.error("", e);
            }
        }
        String printout = scorePerDoc.entrySet().stream().
                map(entry -> entry.getValue() + ": " + entry.getKey()).collect(Collectors.joining("\n"));
        logger.info(printout);
    }

}
