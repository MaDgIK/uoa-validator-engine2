<?xml version="1.0" encoding="UTF-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4.1/metadata.xsd">
  <identifier identifierType="DOI">10.14749/1629884021</identifier>
  <creators>
    <creator>
      <creatorName nameType="Personal">Struwig, Jare</creatorName>
    </creator>
    <creator>
      <creatorName nameType="Personal">Roberts, Benjamin</creatorName>
    </creator>
    <creator>
      <creatorName nameType="Organizational">Human Sciences Research Council</creatorName>
    </creator>
  </creators>
  <titles>
    <title xml:lang="en">South African Social Attitudes Survey (SASAS) 2018: Questionnaire 1 - All provinces</title>
  </titles>
  <publisher>HSRC - Human Science Research Council SA</publisher>
  <publicationYear>2021</publicationYear>
  <resourceType resourceTypeGeneral="Dataset"/>
  <contributors>
    <contributor contributorType="Producer">
      <contributorName nameType="Organizational">Human Sciences Research Council</contributorName>
    </contributor>
  </contributors>
  <dates>
    <date dateType="Available">2021-04-04</date>
    <date dateType="Collected">2018</date>
    <date dateType="Collected">2018/2019</date>
  </dates>
  <version>1.0</version>
  <rightsList>
    <rights xml:lang="en">Other</rights>
    <rights xml:lang="en">By accessing the data, you give assurance that The data and documentation will not be duplicated, redistributed or sold without prior approval from the rights holder. The data will be used for scientific research or educational purposes only. The data will only be used for the specified purpose. If it is used for another purpose the additional purpose will be registered. Redundant data files will be destroyed. The confidentiality of individuals/organisations in the data will be preserved at all times. No attempt will be made to obtain or derive information from the data to identify individuals/organisations. The HSRC will be acknowledged in all published and unpublished works based on the data according to the provided citation. The HSRC will be informed of any books, articles, conference papers, theses, dissertations, reports or other publications resulting from work based in whole or in part on the data and documentation. For archiving and bibliographic purposes an electronic copy of all reports and publications based on the requested data will be sent to the HSRC. To offer for deposit into the HSRC Data Collection any new data sets which have been derived from or which have been created by the combination of the data supplied with other data. The data team bears no responsibility for use of the data or for interpretations or inferences based upon such uses. Failure to comply with the End User License may result in sanctions being imposed.</rights>
  </rightsList>
  <descriptions>
    <description xml:lang="en" descriptionType="Abstract">&lt;p&gt;Description: 
 Topics covered in the questionnaire are: democracy and governance, national identity and pride, intergroup relations, immigrant related attitudes and behaviour, education, moral issues, fatherhood, personal wellbeing index, poverty, taxation, crime and safety, batho pele, voting, respondent characteristics, household characteristics, personal and household income.

 Of the targeted population of 3500, 2885 responses (82.4%) was realized.

 The data set for dissemination contains 2885 cases and 484 variables.
&lt;/p&gt;&lt;p&gt;Abstract: 
 The primary objective of the South African Social Attitudes Survey (SASAS) is to design, develop and implement a conceptually and methodologically robust study of changing social attitudes and values in South Africa. In meeting this objective, the HSRC is carefully and consistently monitoring and providing insight into changes in attitudes among various socio-demographic groupings. SASAS is intended to provide a unique long-term account of the social fabric of modern South Africa, and of how it's changing political and institutional structures interact over time with changing social attitudes and values.

 The survey is conducted annually and the 2018 survey is the sixteenth wave in the series.

 The core module will remain constant for subsequent annual SASAS surveys with the aim of monitoring change and continuity in a variety of socio-economic and socio-political variables. In addition, a number of themes will be accommodated in rotation. The rotating element of the survey consists of two or more topic-specific modules in each round of interviewing and is directed at measuring a range of policy and academic concerns and issues that require more detailed examination at a specific point in time than the multi-topic core module would permit. This dataset focuses specifically on democracy and governance, national identity and pride, intergroup relations, immigrant related attitudes and behaviour, education, moral issues, personal wellbeing index, crime and security, poverty, batho pele, voting, respondent characteristics, household characteristics, personal and household income variables.
&lt;/p&gt;</description>
    <description xml:lang="en" descriptionType="Methods">Face-to-face interview</description>
    <description xml:lang="en" descriptionType="Methods">National Population: Adults (aged 16 and older).</description>
    <description xml:lang="en" descriptionType="Methods">SASAS has been designed to yield a representative sample of 3500 adult South African citizens aged 16 and older (with no upper age limit), in households geographically spread across the country's nine provinces. The sampling frame used for the survey was based on the 2011 census and a set of small area layers (SALs). Estimates of the population numbers for various categories of the census variables were obtained per SAL. In this sampling frame special institutions (such as hospitals, military camps, old age homes, schools and university hostels), recreational areas, industrial areas and vacant SALs were excluded prior to the drawing of the sample.

 Small area layers (SALs) were used as primary sampling units and the estimated number of dwelling units (taken as visiting points) in the SALs as secondary sampling units. In the first sampling stage the primary sampling units (SALs) were drawn with probability proportional to size, using the estimated number of dwelling units in an SAL as measure of size. The dwelling units as secondary sampling units were defined as separate (non-vacant) residential stands, addresses, structures, flats, homesteads, etc. In the second sampling stage a predetermined number of individual dwelling units (or visiting points) were drawn with equal probability in each of the drawn dwelling units. Finally, in the third sampling stage a person was drawn with equal probability from all 16 year and older persons in the drawn dwelling units.

 Three explicit stratification variables were used, namely province, geographic type and majority population group. As stated earlier, within each stratum, the allocated number of primary sampling units (which could differ between different strata) was drawn using proportional to size probability sampling with the estimated number of dwelling units in the primary sampling units as measure of size. In each of these drawn primary sampling units, seven dwelling units were drawn. This resulted in a sample of 2885 individuals.

 A list of the 500 drawn SALs were given to geographic information specialists and maps were then created for each of the 500 areas, indicating certain navigational beacons such as schools, roads churches etc.

 Selection of individuals: For each of the SASAS samples interviewers visited each visiting point drawn in the SALs (PSU) and listed all eligible persons for inclusion in the sample, that is all persons currently aged 16 years or older and resident at the selected visiting point. The interviewer then selected one respondent using a random selection procedure based on a Kish grid.</description>
  </descriptions>
  <geoLocations>
    <geoLocation>
      <geoLocationPlace>South Africa</geoLocationPlace>
    </geoLocation>
  </geoLocations>
  <fundingReferences>
    <fundingReference>
      <funderName>Human Sciences Research Council</funderName>
    </fundingReference>
  </fundingReferences>
</resource>